package com.example.mytestapplication.internet;

import com.google.gson.annotations.SerializedName;

public class PhotoResponse {
    @SerializedName("data")
    public Photo photo;
}

package com.example.mytestapplication.internet;

import com.google.gson.annotations.SerializedName;

public class Photo {
    private int id;
    private int width;
    private int height;
    private String author;
    private String url;
    @SerializedName("download_url")
    private String downloadUrl;

    public Photo(int id, int width, int height, String author, String url, String downloadUrl) {
        this.id = id;
        this.width = width;
        this.height = height;
        this.author = author;
        this.url = url;
        this.downloadUrl = downloadUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public String getThumb() {
        return "https://picsum.photos/id/" + id + "/300/300";
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }
}

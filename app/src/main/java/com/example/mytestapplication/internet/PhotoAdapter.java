package com.example.mytestapplication.internet;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.mytestapplication.R;
import com.example.mytestapplication.ui.PhotoClickListener;

import java.util.List;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder> {
    private List<Photo> photos;
    private PhotoClickListener listener;

    public PhotoAdapter(List<Photo> photos, PhotoClickListener listener) {
        this.photos = photos;
        this.listener = listener;
    }

    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false);
        return new PhotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoViewHolder holder, int position) {
        Photo photo = photos.get(position);
        holder.author.setText(photo.getAuthor());
        holder.url.setText(photo.getUrl());
        Glide.with(holder.itemView).load(photo.getThumb()).into(holder.image);
        holder.itemView.setOnClickListener(view -> listener.onCLick(photo));
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    class PhotoViewHolder extends RecyclerView.ViewHolder {
        private TextView author;
        private TextView url;
        private ImageView image;

        PhotoViewHolder(View itemView) {
            super(itemView);
            author = itemView.findViewById(R.id.author);
            url = itemView.findViewById(R.id.url);
            url.setSelected(true);
            image = itemView.findViewById(R.id.image);
        }
    }
}

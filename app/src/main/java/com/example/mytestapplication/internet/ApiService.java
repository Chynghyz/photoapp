package com.example.mytestapplication.internet;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
    @GET("list")
    Call<List<Photo>> getAllPhotos();
}

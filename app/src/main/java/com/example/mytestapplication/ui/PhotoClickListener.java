package com.example.mytestapplication.ui;

import com.example.mytestapplication.internet.Photo;

public interface PhotoClickListener {
    void onCLick(Photo photo);
}

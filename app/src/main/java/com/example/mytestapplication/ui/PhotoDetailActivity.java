package com.example.mytestapplication.ui;

import android.os.Bundle;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mytestapplication.R;

public class PhotoDetailActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);
        setTitle(getIntent().getStringExtra("author"));
        ((WebView) findViewById(R.id.webview)).loadUrl(getIntent().getStringExtra("photoUrl"));
    }
}

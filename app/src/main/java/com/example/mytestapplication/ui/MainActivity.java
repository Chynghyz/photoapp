package com.example.mytestapplication.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mytestapplication.R;
import com.example.mytestapplication.internet.ApiService;
import com.example.mytestapplication.internet.Photo;
import com.example.mytestapplication.internet.PhotoAdapter;
import com.example.mytestapplication.internet.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private PhotoAdapter adapter;
    private RecyclerView list;
    private ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = findViewById(R.id.list);
        progress = findViewById(R.id.progress);
        getPhotos();
    }

    private void getPhotos() {
        progress.setVisibility(View.VISIBLE);
        ApiService api = new RetrofitClient().getInstance().create(ApiService.class);
        Call<List<Photo>> response = api.getAllPhotos();
        response.enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                progress.setVisibility(View.GONE);
                List<Photo> photos = response.body();
                showPhotos(photos);
                Log.e("log", "onResponse: " + photos.size());
            }

            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {
                progress.setVisibility(View.GONE);
                Log.e("log", "onFailure: " + t.getMessage());
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showPhotos(List<Photo> photos) {
        adapter = new PhotoAdapter(photos, photo -> openPhoto(photo));
        list.setAdapter(adapter);
    }

    private void openPhoto(Photo photo) {
        Intent intent = new Intent(this, PhotoDetailActivity.class);
        intent.putExtra("author", photo.getAuthor());
        intent.putExtra("photoUrl", photo.getUrl());
        startActivity(intent);
    }
}